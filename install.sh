#!/bin/bash

SPARK_VERSION=${SPARK_VERSION:-3.5.0}
SPARK_HOME="/opt/spark"

# Set environment
echo "export SPARK_HOME=$SPARK_HOME" | sudo tee -a $ENVIRONMENT_FILE
source /etc/profile

# Install Apache Spark
# ------------------------------------------------------------------------------------------------------------------------------------------
# Create directories for Hadoop and set permissions
sudo mkdir -p $SPARK_HOME
sudo chown -R $USER $SPARK_HOME

wget https://downloads.apache.org/spark/KEYS -O /tmp/spark.keys
wget https://dlcdn.apache.org/spark/spark-$SPARK_VERSION/spark-$SPARK_VERSION-bin-hadoop3.tgz.asc -O $KEYRINGS_PATH/spark.asc
wget https://dlcdn.apache.org/spark/spark-$SPARK_VERSION/spark-$SPARK_VERSION-bin-hadoop3.tgz -O /tmp/spark.tgz

#gpg --import /tmp/spark.keys
#gpg --verify $KEYRINGS_PATH/spark.asc /tmp/spark.tgz

tar -xzvf /tmp/spark.tgz -C $SPARK_HOME --strip-components=1
